package barcode.test

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import com.google.zxing.Result
import kotlinx.android.synthetic.main.fragment_main.view.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

/**
 * Created by Ruslan Arslanov on 15/08/2018.
 */
class MainFragment : Fragment(), ZXingScannerView.ResultHandler {

    private lateinit var scannerView: ZXingScannerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_main, container, false)
        scannerView = CustomScannerView(context)
        v.root.addView(scannerView)
        return v
    }

    override fun onResume() {
        super.onResume()
        scannerView.setResultHandler(this)
        scannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        scannerView.setResultHandler(null)
        scannerView.startCamera()
    }

    override fun handleResult(rawResult: Result?) {
        val result = rawResult ?: return
        val ctx = context ?: return

        MaterialDialog
            .Builder(ctx)
            .title(R.string.scanning_result)
            .content(result.text)
            .positiveText(R.string.scanning_result_ok_button)
            .onPositive { _, _ -> scannerView.resumeCameraPreview(this@MainFragment) }
            .show()
    }

}