package barcode.test

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import me.dm7.barcodescanner.core.ViewFinderView

/**
 * Created by Ruslan Arslanov on 15/08/2018.
 */
class CustomViewFinderView @JvmOverloads constructor(

    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0

) : ViewFinderView(
    context,
    attrs
) {

    override fun drawLaser(canvas: Canvas?) {
        // Disable drawing laser.
    }

}