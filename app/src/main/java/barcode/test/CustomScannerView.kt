package barcode.test

import android.content.Context
import android.util.AttributeSet
import me.dm7.barcodescanner.core.IViewFinder
import me.dm7.barcodescanner.zxing.ZXingScannerView

/**
 * Created by Ruslan Arslanov on 15/08/2018.
 */
class CustomScannerView @JvmOverloads constructor(

    context: Context?,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0

) : ZXingScannerView(
    context,
    attrs
) {

    override fun createViewFinderView(context: Context?): IViewFinder {
        return CustomViewFinderView(context)
    }

}